import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Grocery } from "../shared/grocery/grocery.model";
import { GroceryService } from "../shared/grocery/grocery.service";
import { TextField } from "tns-core-modules/ui/text-field";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { View } from "tns-core-modules/ui/core/view";

import { RouterExtensions } from "nativescript-angular/router";
import { getString } from "application-settings";
import { alert } from "../shared/dialog-util";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";
import { DatabaseService } from "../database/sqlite.service";
import { Product } from "~/shared/product/product.model";
import { ProductService } from "../shared/product/product.service";

@Component({
    selector: "gr-list",
    templateUrl: "./add-product.component.html",
    styleUrls: ["./add-product.component.css"],
//  TODO: Tiran error tns build android --bundle. Module not found
//    templateUrl: "list/list.component.html",
//    styleUrls: ["list/list.component.css"],
    //providers: [GroceryService, UserService],
    //moduleId: module.id,
})

export class AddProductComponent implements OnInit {
    producto: Product;

    constructor(
        private router: RouterExtensions,
        private database: DatabaseService
    ) { 
        this.producto = new Product();
    }

    ngOnInit(): void { }
    
    add() {
        /*
        this.groceryService.add(this.grocery)
            .subscribe(
            groceryObject => {
                this.groceryList.unshift(groceryObject);
                this.grocery = "";
            },
            () => {
                alert({
                    message: "An error occurred while adding an item to your list.",
                    okButtonText: "OK"
                });
                this.grocery = "";
            }
        )
        */
       console.log(this.producto);
       
       /* COMIENZO DEL ALTA DE PRODUCTOS SQL */
       /*
       if (this.producto.trim() === "") {
           alert("Enter a grocery item");
           return;
        }
        
        let textField = <TextField>this.groceryTextField.nativeElement;
        textField.dismissSoftInput();
        <!--  producto ( nombre VARCHAR(100), sku VARCHAR(100), descripcion TEXT, stock INT(6), precio_u DECIMAL(8,2))-->
        <!--  producto ( nombre VARCHAR(100), sku VARCHAR(100), descripcion TEXT, stock INT(6), precio_u DECIMAL(8,2))-->
        */

//        this.db.execSQL("INSERT INTO productos (item_name, user_id) VALUES (?,?)", [this.grocery, this.user_id])
        this.database.getDbConnection()
		.then(db => {
           console.log(this.producto);
            db.execSQL("INSERT INTO producto (nombre, sku, descripcion, stock, precio_u) VALUES (?,?,?,?,?)", [this.producto.nombre, this.producto.sku, this.producto.descripcion, this.producto.stock, this.producto.precio_u])
            .then(id => {
                alert("El producto fue agregado exitosamente");
                this.router.navigate(["list"]);
            }, error => {
                alert('An error occurred while adding an item to your list.');
               console.log(error);

                this.producto = new Product();
            });
        });
       /* */
    }
}