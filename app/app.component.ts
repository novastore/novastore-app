import { Component } from "@angular/core";
import { DatabaseService } from "./database/sqlite.service";

@Component({
  selector: "gr-app",
  template: "<page-router-outlet></page-router-outlet>",
  providers: [ DatabaseService ]
})

export class AppComponent { 
	constructor(private database: DatabaseService) {
		this.database.getDbConnection()
		.then(db => {
			//db.execSQL("DROP TABLE product");
			//db.execSQL("DROP TABLE user");
			db.execSQL("CREATE TABLE IF NOT EXISTS producto ( id INTEGER PRIMARY KEY AUTOINCREMENT, nombre VARCHAR(100), sku VARCHAR(100), descripcion TEXT, stock INT(6), precio_u DECIMAL(8,2))")
			.then(() => {
			}, error => {
				console.log('CREATE TABLE error', error);
			});

			db.execSQL("CREATE TABLE IF NOT EXISTS usuario (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, password TEXT)")
			.then(() => {
			}, error => {
				console.log('CREATE TABLE error', error);
			});
		});
	} 
}