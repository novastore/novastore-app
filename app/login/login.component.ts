import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { setString } from "application-settings";
import 'rxjs/add/operator/toPromise';

import { alert } from "../shared/dialog-util";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";

@Component({
    selector: "Login",
    moduleId: module.id,
    providers: [UserService],
    styleUrls: ["./login.component.css"],
    templateUrl: "./login.component.html"
//  TODO: Tiran error tns build android --bundle. Module not found
//    styleUrls: ["login/login.component.css"],
//    templateUrl: "login/login.component.html"
})
export class LoginComponent implements OnInit {
    user: User;
    isLoggingIn = true;
    isBusy = false;

    constructor(private router: Router, 
                private routerExtensions: RouterExtensions,
                private userService: UserService,
                private page: Page
    ) {
        this.user = new User();
    }

    ngOnInit() {
        this.page.actionBarHidden = true;
    }

    formValidLogin():boolean {
        return this.user.email != "" && this.user.password != "";
    }

    formValidRegister():boolean {
        return this.user.email != "" && this.user.password != "" && this.user.repassword != "";
    }

    submit() {
        if (this.isLoggingIn) {
            this.login();
        } else {
            this.signUp();
        }

        this.isBusy = false;
    }

    login() {
        this.isBusy = true;
        if (this.verifyFieldsLogin()) {
            this.userService.login(this.user)
            .then(status => {
                this.isBusy = false;
                setString("user_id", this.user.email);
                this.routerExtensions.navigate(["/list"], { clearHistory: true });
            }, err => {
               this.clearFields();
               alert("No se ha podido verificar su cuenta.");
            });
        }
    }

    signUp() {
        this.isBusy = true;
        if (this.verifyFieldsRegister()) {
            this.userService.register(this.user)
            .then(status => {
                this.toggleDisplay();
                this.clearFields();
                alert("Su cuenta ha sido generada.");
            }, err => {
                this.clearFields();
                console.log(err);
                alert("Lamentablemente no se pudo crear su cuenta.")
            }).catch(e => {
                console.log(e);
            });
        }
    }

    clearFields() {
		this.user.email = '';
		this.user.password = '';
		this.user.repassword = '';
	}

    toggleDisplay() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    verifyFieldsLogin() {
        if ( !this.user.isValidEmail() ) {
            alert("Por favor ingrese un email válido.");
			return;
		}else if (!this.user.email) {
            alert("Ingrese una cuenta de email.");
            return false;
        }else if (!this.user.password) {
            alert("Ingrese una password.");
            return false;
        }

        return true;
    }

    verifyFieldsRegister() {
        if ( !this.user.isValidEmail() ) {
            alert("Por favor ingrese un email válido.");
			return;
		}else if (!this.user.email) {
            alert("Ingrese una cuenta de email.");
            return false;
        }else if (!this.user.password) {
            alert("Ingrese una password.");
            return false;
        }else if (!this.user.repassword) {
            alert("Confirme su password.");
            return false;
        }else if (this.user.password != this.user.repassword) {
            alert("La password no coincide.");
            return false;
        }

        return true;
    }
}