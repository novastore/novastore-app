import { LoginComponent } from "./login/login.component";
import { ListComponent } from "./list/list.component";
import { AddProductComponent } from "./add-product/add-product.component";

export const routes = [
    { path: "", component: LoginComponent },
    { path: "list", component: ListComponent },
    { path: "add-product", component: AddProductComponent }
];

export const navigatableComponents = [
    LoginComponent,
    ListComponent,
    AddProductComponent
];