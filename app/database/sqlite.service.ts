
import { Injectable } from "@angular/core";

var Sqlite = require("nativescript-sqlite");

@Injectable()
export class DatabaseService {
	
	public getDbConnection(){
		return new Sqlite('nova_store');
	}

	public closeDbConnection(){
		new Sqlite('nova_store')
		.then(
			(db) => {
				db.close();
			});
	}
}