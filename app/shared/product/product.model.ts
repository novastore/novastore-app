export class Product {
    id: number;
    nombre: string;
    sku: string;
    descripcion: string;
    stock: number;
    precio_u: number;

    constructor() { 
        this.id = null;
        this.nombre = '';
        this.sku = '';
        this.descripcion = '';
        this.stock = null;
        this.precio_u = null;
    }
}