import * as dialogsModule from "ui/dialogs";

export function alert(message: string) {
    return dialogsModule.alert({
        title: "Nova Store",
        okButtonText: "OK",
        message: message
    });
}