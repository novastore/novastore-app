import * as validator from 'email-validator';

export class User {
    email: string;
    password: string;
    repassword: string;

    constructor(){
        this.email = '';
        this.password = '';
        this.repassword = '';        
    }
    isValidEmail() {
        return validator.validate(this.email);
    }
}