import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable, throwError } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import { User } from "./user.model";
import { Config } from "../config";
import { DatabaseService } from "../../database/sqlite.service";
import { BackendService } from "../backend.service";

@Injectable()
export class UserService {
    constructor(private http: Http,
                private database: DatabaseService) { }

    register(user: User) {
		return new Promise<object>((resolve, reject) => {
			this.database.getDbConnection()
			.then(db => {
				db.execSQL("INSERT INTO usuario (email, password) VALUES (?,?)", [user.email, user.password])
				.then(id => {
					resolve({ "status": true });
				}, err => {
					reject({ "status": false });
				});
			});
		});
    }

    login(user: User) {

        return new Promise<Object>((resolve, reject) => {

            this.database.getDbConnection()
                .then(db => {
                    db.all("SELECT * FROM usuario where email like'" + user.email + "' and password like '" + user.password + "'").then(rows => {
                        if (rows.length > 0) {
                            BackendService.token = "dummy_token";
                            resolve({ status: true });
                        }
                        else {
                            reject({ status: false });
                        }
                    });
                });
        });
    }

    logout(user: User) {
		BackendService.token = "";
		this.database.closeDbConnection();
	}
}