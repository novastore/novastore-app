import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { View } from "tns-core-modules/ui/core/view";

import { RouterExtensions } from "nativescript-angular/router";
import { getString } from "application-settings";
// import { alert } from "../shared/dialog-util";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";
import { DatabaseService } from "../database/sqlite.service";
import { Product } from "~/shared/product/product.model";

@Component({
    selector: "gr-list",
    templateUrl: "./list.component.html",
    styleUrls: ["./list.component.css"],
//  TODO: Tiran error tns build android --bundle. Module not found
//    templateUrl: "list/list.component.html",
//    styleUrls: ["list/list.component.css"],
    providers: [ UserService],
    moduleId: module.id,
})

export class ListComponent implements OnInit {
    productList: Array<Product> = [];
    db: any;
    user_id: string;
    isLoading = false;
    listLoaded = false;

    constructor(
        private routerExtensions: RouterExtensions,
        private userService: UserService,
        private database: DatabaseService
    ) {
        this.user_id = getString("user_id");
    }

    ngOnInit(): void {
        this.selectItems();
    }
    
    selectItems() {
        this.database.getDbConnection()
        .then(db => {
            db.all("SELECT * FROM producto LIMIT 10")
            .then(rows => {
                for (var row in rows) {
                    this.productList.push({ 
                        id: rows[row][0],
                        nombre: rows[row][1],
                        sku: rows[row][2],
                        descripcion: rows[row][3],
                        stock: rows[row][4],
                        precio_u: rows[row][5],
                    });
                }
                
                this.db = db;
            }, error => {
                console.log("SELECT ERROR", error);
            });
        });
    }

    onSwipeCellStarted(args: ListViewEventData) {
        var swipeLimits = args.data.swipeLimits;
        var swipeView = args.object;
        var rightItem = swipeView.getViewById<View>("delete-view");

        swipeLimits.right = rightItem.getMeasuredWidth();
        swipeLimits.left = 0;
        swipeLimits.threshold = rightItem.getMeasuredWidth() / 2;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        //const sideDrawer = <RadSideDrawer>app.getRootView();
        //sideDrawer.closeDrawer();
    }

    delete(args: ListViewEventData) {
        let product = <any>args.object.bindingContext;

        this.db.execSQL("DELETE FROM producto WHERE id=?", [product.id])
        .then(() => {
            let index = this.productList.indexOf(product);
            this.productList.splice(index, 1);
        });
    }
}